package com.example.test;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Date;

public class DetailsUser extends AppCompatActivity {

    private static final String TAG = "DetailsUser";

    String Name, Ingredient, Instruction;

    EditText recipeName, ingredientIn, instructionIn;

    Button submitButton;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_user);




        recipeName =  (EditText) findViewById(R.id.recipeName);
        ingredientIn = (EditText) findViewById(R.id.ingredients);
        instructionIn = (EditText) findViewById(R.id.instructions);



     /*   submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        }); */
    }
    public void submit(){
        initialize();
    }

    public void initialize(){
        Name = recipeName.getText().toString().trim();
        Ingredient = ingredientIn.getText().toString().trim();
        Instruction = instructionIn.getText().toString().trim();
        String concatenatedText = Name + Ingredient + Instruction;

        submitButton = (Button) findViewById(R.id.submit);

      //  Toast.makeText(this, "Recipe added", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Navbar.class);
        startActivity(intent);


    }
    @Override
    protected void onPause(){

        super.onPause();
        SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("recipe", recipeName.getText().toString());
        editor.putString("ingredient", ingredientIn.getText().toString());
        editor.putString("instruction", instructionIn.getText().toString());
        editor.putString("date", Calendar.getInstance().getTime().toString());
        editor.apply();

        Log.wtf(TAG, "onPause");


    }
    @Override
    protected void onResume(){
        super.onResume();

        SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        Name = sharedPref.getString("recipe", recipeName.getText().toString());
        Ingredient = sharedPref.getString("ingredient", ingredientIn.getText().toString());
        Instruction = sharedPref.getString("instruction", instructionIn.getText().toString());

        String currentTime = Calendar.getInstance().getTime().toString();
        String date = sharedPref.getString("date",currentTime);

        View parentLayout = findViewById(R.id.fab);
        Snackbar.make(parentLayout, date, Snackbar.LENGTH_LONG).setAction("CLOSE", null).show();

        recipeName.setText(Name);
        ingredientIn.setText(Ingredient);
        instructionIn.setText(Instruction);

        Log.wtf(TAG, "onResume");
    }
    @Override
    protected void onStop(){
        super.onStop();
       SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("recipe", recipeName.getText().toString());
        editor.putString("ingredient", ingredientIn.getText().toString());
        editor.putString("instruction", instructionIn.getText().toString());
        editor.putString("date", Calendar.getInstance().getTime().toString());

        editor.apply();
        Log.wtf(TAG, "onStop");

    }
    @Override
    protected void onStart(){
        super.onStart();

       SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        Name = sharedPref.getString("recipe", recipeName.getText().toString());
        Ingredient = sharedPref.getString("ingredient", ingredientIn.getText().toString());
        Instruction = sharedPref.getString("instruction", instructionIn.getText().toString());




        String currentTime = Calendar.getInstance().getTime().toString();
        String date = sharedPref.getString("date",currentTime);

        View parentLayout = findViewById(R.id.fab);
        Snackbar.make(parentLayout, date, Snackbar.LENGTH_LONG).setAction("CLOSE", null).show();



        recipeName.setText(Name);
        ingredientIn.setText(Ingredient);
        instructionIn.setText(Instruction);

        Log.wtf(TAG, "onStart");

    }



}
