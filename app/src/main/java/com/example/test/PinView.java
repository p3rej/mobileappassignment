package com.example.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import android.content.Intent;

import com.example.test.Navbar;
import com.example.test.R;
import com.goodiebag.pinview.Pinview;

public class PinView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_view);
        final String password = "3659";
        Pinview pinview = (Pinview)findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean h) {
                if(pinview.getValue().equals(password) == true){
                    openMainMenu();
                    Toast.makeText(PinView.this, ""+pinview.getValue(), Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(PinView.this, "Incorrect Pin Code....", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void openMainMenu(){
        Intent intent = new Intent(this, Navbar.class);
        startActivity(intent);
    }
}
