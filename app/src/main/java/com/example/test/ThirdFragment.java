package com.example.test;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ThirdFragment extends Fragment
{
    View myView;

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.activity_details_user, container, false);
        openMainMenu();
        return myView;
    }

    public void openMainMenu(){
        Intent intent = new Intent(getActivity(), DetailsUser.class);
        startActivity(intent);
    }
}
