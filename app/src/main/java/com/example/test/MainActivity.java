package com.example.test;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;


import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private  ExampleAdapter mExampleAdapter;
    private ArrayList<ExampleItem> mExampleList;
    private RequestQueue mRequestQueue;


    String News_Url;
    private static final String TAG = "DetailsUser";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mExampleList = new ArrayList<>();

        mRequestQueue = Volley.newRequestQueue(this);


        News_Url = "https://www.food2fork.com/api/search?key=302ff9deb77f1e7e2c101ba36c9b2928";

        new MainActivity.AsyncHttpTask().execute(News_Url);

        openMainMenu();



    }

    public class AsyncHttpTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL (urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                String response = streamToString(urlConnection.getInputStream());
                parseResult(response);

                return result;


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    String streamToString(InputStream stream) throws IOException{
        //BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        BufferedReader bufferesReader = new BufferedReader(new InputStreamReader(stream));
        String data;
        String result = "";

        while((data = bufferesReader.readLine()) != null) {

            result += data;
        }
        if(null != stream){

            stream.close();
        }

        return result;
    }

    private void parseResult(String result){

        JSONObject response = null;

        try {
            response = new JSONObject(result);
            JSONArray articles = response.optJSONArray("recipes");

            for(int i = 0; i < articles.length(); i++){

                JSONObject article = articles.optJSONObject(i);

                String imageUrl = article.optString("image_url");
                String title = article.optString("title");


                mExampleList.add(new ExampleItem(imageUrl, title));


                Log.i("Titles", title);
                Log.i("Images", imageUrl);
            }

            mExampleAdapter = new ExampleAdapter(MainActivity.this, mExampleList);
            mRecyclerView.setAdapter(mExampleAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public void openMainMenu(){
        Intent intent = new Intent(this, Navbar.class);
        startActivity(intent);
    }



}

